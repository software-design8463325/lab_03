class Hero:
    def __init__(self, name):
        self.name = name
        self.inventory = []

    def equip(self, item):
        self.inventory.append(item)

    def show_inventory(self):
        print(f"{self.name}'s Inventory:")
        for item in self.inventory:
            print("-", item)


class ItemDecorator:
    def __init__(self, item):
        self.item = item

    def equip(self, hero):
        pass

    def show_inventory(self):
        pass


class Sword(ItemDecorator):
    def equip(self, hero):
        hero.equip("Sword")


class Armor(ItemDecorator):
    def equip(self, hero):
        hero.equip("Armor")


class Ring(ItemDecorator):
    def equip(self, hero):
        hero.equip("Ring")


class Necklace(ItemDecorator):
    def equip(self, hero):
        hero.equip("Necklace")


def main():
    # Create heroes
    warrior = Hero("Warrior")
    mage = Hero("Mage")
    paladin = Hero("Paladin")

    # Equip items
    Sword(warrior).equip(warrior)
    Armor(warrior).equip(warrior)

    Ring(mage).equip(mage)
    Necklace(mage).equip(mage)

    Sword(paladin).equip(paladin)
    Armor(paladin).equip(paladin)
    Ring(paladin).equip(paladin)

    # Show inventories
    warrior.show_inventory()
    print()
    mage.show_inventory()
    print()
    paladin.show_inventory()


if __name__ == "__main__":
    main()
