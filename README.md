## Design Patterns Implemented in Python

This repository contains several examples of design patterns implemented in Python. Each example demonstrates a different design pattern, providing both the implementation and usage of the pattern.

### Table of Contents
1. Adapter Pattern
2. Decorator Pattern
3. Bridge Pattern
4. Proxy Pattern
5. Composite Pattern
6. LightHTML Custom Parser

---

### Adapter Pattern

**Classes:**
- `Logger`
- `FileWriter`
- `FileWriterAdapter`

**Description:**
The Adapter pattern is used to allow two incompatible interfaces to work together. In this example, `FileWriterAdapter` adapts the `FileWriter` class to the `Logger` interface. This allows logging messages to be written to a file instead of being printed to the console.

---

### Decorator Pattern

**Classes:**
- `Hero`
- `ItemDecorator`
- `Sword`
- `Armor`
- `Ring`
- `Necklace`

**Description:**
The Decorator pattern is used to extend the functionality of objects dynamically. This example demonstrates how a hero can be equipped with various items, each adding to their inventory.

---

### Bridge Pattern

**Classes:**
- `Renderer`
- `VectorRenderer`
- `RasterRenderer`
- `Shape`
- `Circle`
- `Square`
- `Triangle`

**Description:**
The Bridge pattern decouples an abstraction from its implementation so that the two can vary independently. This example shows how different shapes can be rendered using different rendering methods.

---

### Proxy Pattern

**Classes:**
- `SmartTextReader`
- `SmartTextChecker`
- `SmartTextReaderLocker`

**Description:**
The Proxy pattern provides a surrogate or placeholder for another object to control access to it. In this example, `SmartTextReaderLocker` restricts access to certain files based on a regex pattern.

---

### Composite Pattern

**Classes:**
- `LightNode`
- `LightElementNode`
- `LightTextNode`

**Description:**
The Composite pattern allows individual objects and compositions of objects to be treated uniformly. In this example, `LightElementNode` and `LightTextNode` are used to build a simple HTML-like structure.

---

### LightHTML Custom Parser

**Classes:**
- `LightHTML`

**Description:**
This is a custom parser that processes text and converts it into HTML. The parser formats lines of text into HTML tags based on their length and leading whitespace.

---
