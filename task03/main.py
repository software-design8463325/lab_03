class Renderer:
    def render(self, shape_name):
        pass

class VectorRenderer(Renderer):
    def render(self, shape_name):
        print(f"Drawing {shape_name} as vector")

class RasterRenderer(Renderer):
    def render(self, shape_name):
        print(f"Drawing {shape_name} as pixels")

class Shape:
    def __init__(self, renderer):
        self.renderer = renderer

    def draw(self):
        pass

class Circle(Shape):
    def __init__(self, renderer):
        super().__init__(renderer)

    def draw(self):
        self.renderer.render("Circle")

class Square(Shape):
    def __init__(self, renderer):
        super().__init__(renderer)

    def draw(self):
        self.renderer.render("Square")

class Triangle(Shape):
    def __init__(self, renderer):
        super().__init__(renderer)

    def draw(self):
        self.renderer.render("Triangle")

def main():
    vector_renderer = VectorRenderer()
    raster_renderer = RasterRenderer()

    shapes = [Circle(vector_renderer), Square(raster_renderer), Triangle(vector_renderer)]

    for shape in shapes:
        shape.draw()

if __name__ == "__main__":
    main()
