class Logger:
    def Log(self, message):
        print("\033[92m" + message + "\033[0m")  # Green color

    def Error(self, message):
        print("\033[91m" + message + "\033[0m")  # Red color

    def Warn(self, message):
        print("\033[93m" + message + "\033[0m")  # Yellow color

class FileWriter:
    def Write(self, message):
        print(f"Writing: {message}")

    def WriteLine(self, message):
        print(f"Writing line: {message}")

class FileWriterAdapter(Logger):
    def __init__(self, file_writer):
        self.file_writer = file_writer

    def Log(self, message):
        self.file_writer.WriteLine(message)

    def Error(self, message):
        self.file_writer.WriteLine(message)

    def Warn(self, message):
        self.file_writer.WriteLine(message)

def main():
    # Creating a FileWriter instance
    file_writer = FileWriter()

    # Creating a FileWriterAdapter instance
    logger_adapter = FileWriterAdapter(file_writer)

    # Using the FileWriterAdapter methods
    logger_adapter.Log("This is a log message")
    logger_adapter.Error("This is an error message")
    logger_adapter.Warn("This is a warning message")

if __name__ == "__main__":
    main()
