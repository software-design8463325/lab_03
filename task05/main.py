class LightNode:
    def __init__(self):
        pass

class LightElementNode(LightNode):
    def __init__(self, tag_name, display_type, closing_type, css_classes=None):
        super().__init__()
        self.tag_name = tag_name
        self.display_type = display_type
        self.closing_type = closing_type
        self.css_classes = css_classes if css_classes else []
        self.children = []

    def add_child(self, child):
        self.children.append(child)

    def outer_html(self):
        attributes = ' '.join(f'{key}="{value}"' for key, value in self.__dict__.items() if key not in ['children', 'tag_name'])
        opening_tag = f'<{self.tag_name} {attributes}>'
        closing_tag = f'</{self.tag_name}>' if self.closing_type == 'dual' else f'<{self.tag_name}/>'
        inner_html = ''.join(child.outer_html() for child in self.children)
        return opening_tag + inner_html + closing_tag

    def inner_html(self):
        return ''.join(child.outer_html() for child in self.children)

class LightTextNode(LightNode):
    def __init__(self, text):
        super().__init__()
        self.text = text

    def outer_html(self):
        return self.text

# Приклад використання

# Створення структури сторінки
html_body = LightElementNode('body', 'block', 'dual')
heading = LightElementNode('h1', 'block', 'dual')
heading.add_child(LightTextNode('Hello, LightHTML!'))
paragraph = LightElementNode('p', 'block', 'dual')
paragraph.add_child(LightTextNode('This is a paragraph in LightHTML.'))

html_body.add_child(heading)
html_body.add_child(paragraph)

# Виведення в консоль outerHTML та innerHTML структури сторінки
print("Outer HTML of the page:")
print(html_body.outer_html())
print("\nInner HTML of the page:")
print(html_body.inner_html())
